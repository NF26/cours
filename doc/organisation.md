# Organisation du dépôt et conventions à utiliser

Le poly de NF26 est compilé avec LaTex, mais contient également des figures
générées par d'autres programmes, comme Inkscape ou PlantUML.

## Chapitres

Le texte des chapitres se trouve dans le dossier `chapters`. Chacun des fichiers
du dossier est ensuite inclus dans le `main.tex`.

## Figures dans le LaTeX

Règle générale: **Toutes les figures doivent être enregistrées dans le dossier
`figs/` quel que soit leur format.**

Les formats supportés actuellement sont:

 - pdf et png (évidement)
 - svg
 - plantuml

### Figures en pdf ou en png

 1. Mettre la figure dans le dossier `figs/`, par exemple: `figs/lapin.pdf`
 1. L'utiliser dans le document LaTeX sans mettre ni le dossier, ni l'extension:

    ```
    \includegraphics[width=.5\textwidth]{lapin}
    ```

### Figures en svg

 1. Mettre la figure dans le dossier `figs/`, par exemple: `figs/moutarde.svg`
 1. L'ajouter à la liste des figures à générer avec l'extension `pdf` dans le
    `Makefile`:

    ```
    GENERATED_PDF_FROM_SVG= \
    ...
          generated/figs/svg/moutarde.pdf \
    ...
    ```
 1. L'utiliser dans le document LaTeX sans mettre ni le dossier, ni l'extension:

    ```
    \includegraphics[width=.5\textwidth]{moutarde}
    ```

### Figures en plantuml (extension `.pu` obligatoire)

 1. Mettre la figure dans le dossier `figs/`, par exemple: `figs/citron.pu`
 1. L'ajouter à la liste des figures à générer avec l'extension `pdf` dans le
    `Makefile`:

    ```
    GENERATED_PDF_FROM_UML= \
    ...
          generated/figs/uml/citron.pdf \
    ...
    ```
 1. L'utiliser dans le document LaTeX sans mettre ni le dossier, ni l'extension:

    ```
    \includegraphics[width=.5\textwidth]{citron}
    ```
