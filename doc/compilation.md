# Compiler le poly de NF26 sous Linux

## Fedora

Tout d'abord, on installe les dépendances. La commande suivante installera tout
pour vous:

```
sudo dnf -y update && sudo dnf -y install \
        glibc-langpack-fr \
        inkscape \
        make \
        plantuml \
        python \
        texlive-collection-basic \
        texlive-collection-latex \
        texlive-algorithm2e \
        texlive-babel-french \
        texlive-doublestroke \
        texlive-ec \
        texlive-ifoddpage \
        texlive-mdframed \
        texlive-minted \
        texlive-stmaryrd \
        texlive-todonotes
```

Ensuite, on clone ce dépôt, on se place dans le dossier cloné et on exécute
`make main.pdf`.

## Troubleshoot

Si lors de l'exécution de `make` on rencontre des erreurs du type:

```
! LaTeX Error: File \`ifoddpage.sty' not found.`
```

alors un paquet LaTeX est manquant (ici, c'est `texlive-ifoddpage`). Il faut
l'installer avant de continuer.

# Édition avec TeXstudio

[TeXstudio](https://www.texstudio.org/) est un éditeur LaTeX dédié, riche en
fonctionnalités. Pour l'utiliser avec ce projet:

1. Compiler le poly en suivant les instructions de la section précédente
1. Ouvrir `main.tex` avec TeXstudio
1. Aller dans Options > Configure TeXstudio > Commands
1. Mettre `pdflatex -shell-escape -halt-on-error %.tex` en tant que commande
   PdfLaTeX
1. Click sur `Build & View` (bouton avec deux triangles verts superposés)
