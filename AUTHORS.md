# Enseignants
 - Jean-Benoist Leger

# Étudiants P2018
 - Florent Chehab
 - Julien Jerphanion
 - Guillaume Jorandon
 - Thomas Le Vaou
 - Guillaume Jounel
 - Léna Schofield
 - Maxime Lucas
 - Emmanuelle Lejeail

# Étudiants P2019
 - Mathis Chenuet

# Étudiants P2020
 - Gaëtan Blond
 - Andres Maldonado
