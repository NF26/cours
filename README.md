# Cours de NF26, partie 2 : Apprentissage en haute volumétrie

## Sur le polycopié collaboratif :

La dernière version du PDF sur `master` est disponible
[ici](/../builds/artifacts/master/raw/main.pdf?job=building-latex) ou
[ici](https://nf26.gitlab.utc.fr/cours/main.pdf). Celle-ci est mise à jour à
chaque commit sur `master`.

## Contribuer au poly

Tout d'abord, on crée une copie de ce projet en cliquant sur `Fork` (`Créer une
divergence` sur la version française). Ensuite, il est possible de faire des
modifications directement depuis l'interface web de GitLab, ou bien de tout
télécharger et travailler en local.

* [Guide pour compiler le poly sous Linux](doc/compilation.md)
* [Organisation du dépôt et conventions à utiliser](doc/organisation.md)

Une fois que nous avons fait nos modifications et vérifié qu'il n'y a pas
d'erreurs de compilation (vous devez voir une balise verte `Passed` / `Vérifié`
à côté du dernier commit)
