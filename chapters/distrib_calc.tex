\chapter{Distribution des calculs}

  \section{Introduction et exemple}

    Exemple: Vous devez sommer les identifiants de $n$ étudiants:
    comment faire ?
    \\
    Solution 1: une personne fait le tour des participants.
    Cette méthode demande $n-1$ opérations (et donc $n-1$ temps
    d'opération).
    \\
    Solution 2: on décompose le problème en deux sommes sur deux groupes,
    puis on fait la somme des deux groupes. Et ainsi de suite sur les
    sous-groupes récursivement jusqu'à ce qu'il n'y ait que deux éléments
    dans le sous-groupe, auquel cas on fait la somme des deux éléments.
    Cette solution demande toujours $n-1$ opérations, mais diminue le
    temps de calcul à $\lceil \log_{2} n \rceil$ si les opérations sur des
    groupes différents peuvent s'effectuer en parallèle.

    \begin{warningBox}
    Ceci est valide car l'opération employée est
    associative et commutative.
    \end{warningBox}
    Rappel de l'associativité: $(a+b)+c=a+(b+c)$ \\
    Rappel de la commutativité: $a+b=b+a$ \\
    La commutativité permet de ne pas ralentir les temps de calcul globaux,
    même quand un nœud met du temps à fournir une information.

  \section{Réduction}

    \begin{warningBox}
      \textbf{Définition:} Soit $\bot$ un opérateur associatif
    et commutatif. Soit $(a_i)_{i \in [1,n]}$ un ensemble de valeurs. Soit
    $b_i$ défini par récurrence comme étant: \[
    \left\{
        \begin{matrix}
            \forall i \geq 1, b_i, b_i &= b_{i-1} \bot a_i \\
            b_0 &= a_0
        \end{matrix}
    \right.
    \]
    \end{warningBox}

    On nomme $b_n$ la réduction de $(a_i)_i$ par $\bot$:
    $b_n=(...(((a_0 \bot a_1) \bot a_2) \bot a_3)... \bot a_n)$ et donc
    par associativité
    $b_n = a_0 \bot a_i \dots \bot a_n$, que l'on peut noter
    $b_n = \bot_{i=0}^n a_i$.


    \begin{infoBox} Parenthèse sur les réductions infinies:
    \(\frac{1}{2} - \frac{1}{3} + \frac{1}{4} - \frac{1}{5} + \frac{1}{6}  - \frac{1}{7} + \frac{1}{8}  \dots\)
    et
    \(\frac{1}{2} + \frac{1}{4} - \frac{1}{3} + \frac{1}{6} + \frac{1}{8}  - \frac{1}{5} - \frac{1}{7}  \dots\)
    ne convergent pas vers la même valeur !
    \\
    En effet \( \sum_{i \in \N^\star} \frac{(-1)^i}{i} \)
    est un abus de notation, ce n'est pas univoque. Une réduction
    s'applique sur un ensemble \textbf{fini}, à la différence d'une série.
    \end{infoBox}

    Exemple: Nous avons un ensemble de données ${a_i}_{i \in I}$ fini,
    avec $\forall i, a_i \in \R$. On peut calculer la moyenne et
    l'écart-type, en utilisant la réduction, de la manière suivante:
    \[
    \bar{x} = \frac{\sum_{i\in I} x_i}{\sum_{i\in I} 1}
    \qquad
    s^2 = \frac{\sum_{i\in I} (x_i - \bar{x})^2}{\sum_{i\in I} 1}
    \]
    Cependant pour paralléliser toutes les réductions nous devrions plutôt utiliser:
    \[
    s^2 = \frac{\sum_{i\in I} x_i^2}{\sum_{i\in I} 1}  -
    \left(\frac{\sum_{i\in I} x_i}{\sum_{i\in I} 1}\right)^2
    \] Sauf que ce n'est pas numériquement stable ! Il faut donc faire:

    \begin{successBox}
      \[
      s^2 = \frac{\sum_{i\in I} (x_i - k)^2}{\sum_{i\in I} 1} - \left( \frac{\sum_{i\in I} x_i}{\sum_{i\in I} 1} - k\right)^2
      \]
      On choisi $k$ comme l'ordre de grandeur des données.
    \end{successBox}

    \subsubsection{Éléments neutres}
      $\bot$ a un élément neutre $e \iff \forall a, a \bot e = a$ et $e \bot a = a$.

      Exemples:
      \begin{center}
        \begin{tabular}{cc|c}
          & sur $\R$ & sur $\bar\R$ \\
          \hline
          + & 0 & 0 \\
          $\times$ & 1 & 1 \\
          $\min$ & & $+\infty$ \\
          $\max$ & & $-\infty$ \\
        \end{tabular}
      \end{center}

  \section{Map Reduce: concept et technologie}

    Association de mapping et réduction.

    \subsection{Map}

      Transformation de la donnée en couple $k/v$:
      \[
      m: \quad d \mapsto (k_i, v_i) = (m_k(d_i), m_v(d_i))
      \]

    \subsection{Reduction}

      Soit $\mathbb{K} = \{m_k(d_i)\}_i$. Pour $k \in \mathbb{K}$,
      $
      k: \underset{i\ \vert \ n_k(d_i)=k}{\bot}
      $
      (on fait une réduction par valeur de la clef.)

    \subsection{Exemple, comptage de mots}

      Le comptage de mots est en quelque sorte le « Hello World » de l'analyse
      de données (exemple facile à comprendre vérifiant le bon fonctionnement de
      la techno).
      Nous analyserons le texte suivant: \emph{'ô rage, ô désespoir, ô vieillesse\dots'}
      \\
      Nous appliquons le mapping à chaque mot: $\text{mot} \mapsto (\text{mot},1)$. Réduction: $+$
      \begin{gather*}
      \{ ('ô',1), \;
      ('rage',1), \;
      ('ô',1), \;
      ('désespoir',1), \;
      ('ô',1), \;
      ('vieillesse',1), \;
      \dots
      \}
      \\
      \text{avec } \mathbb{K} = \{'ô', \; 'rage', \; 'désespoir', \; 'vieillesse', \;  \dots \}
      \end{gather*}
      Résultat: $\{
          'ô': 3,
          'rage': 1,
          'désespoir': 1,
          ...
      \}$

    \subsection{Dans les technologies}

      Cela est emprunté aux langages fonctionnel. Des langages propose de
      telle API sur leur collections, c'est le cas de \textit{Scala} par exemple
      sur lequel se base \textit{Spark}

      \begin{scala}
      // Soit deux vecteurs
      val vec1: List[Int] = List(1,2,4,3,5,6,1)
      val vec2: List[Int] = List(3,0,3,1,5,9,1)

      // Pour faire leur produit scalaire on peut faire
      // un `map` sur leur association (via `zip`) suivi d'un `reduce` :
      val prodScal: Int = vec1.zip(vec1,vec2)
                              .map((elem1,elem2) => elem1 * elem2)
                              .reduce((compo1,compo2) => compo1 + compo2)

      // ou de manière plus concise :
      val prodScalPrime: Int = vec1.zip(vec1,vec2)
                              .map(_ * _)
                              .reduce(_ + _) // voir .sum simplement
      \end{scala}

      \begin{infoBox} Voir la doc des API de la \texttt{List} en Scala pour se faire
      une idée:
      \url{https://www.scala-lang.org/api/current/scala/collection/immutable/List.html}
      \end{infoBox}

      Ce sont les mêmes API qui l'on retrouve avec \textit{Spark} sur des
      collections distribuées appelées RDD (\textit{Resilient Distributed Dataset}).
      Il y a plein d'autres \textit{transformations} et \textit{opérations}
      possibles comme \texttt{fold}, \texttt{aggregate}, \texttt{foldLeft},
      \texttt{count}, \texttt{flatMap} et d'autres.
      \footnote{Voir la doc de l'API des RDD pour Scala:
      \url{https://spark.apache.org/docs/2.3.0/api/scala/index.html\#org.apache.spark.rdd.RDD}}
      L'API Python propose la même chose pour Spark via \textit{PySpark}.
      \footnote{Voir la doc de l'API des RDD pour Python:
      \url{https://spark.apache.org/docs/2.3.0/api/python/pyspark.html\#pyspark.RDD}}

\section{Illustration de différents exemples de mapping-réduction.}

\textbf{Rappel sur les données:}
$
  \braces{\p{x_i, z_i}}_i\; \forall i,\,x_i\in\R,\, z_i\in\intEe0K
$

\subsection{Médiane}

Calculer directement la médiane de manière naïve est coûteux, par exemple en triant toutes les données.
Cependant on peut savoir facilement si un nombre $P_M$ (proposition de médiane) est inférieur ou supérieur à la médiane.
\\
Objectif: calculer \[\frac{\sum_i \indicatrice{truc_i \le P_M}}{\sum_i 1} = F_k(P_M)\]
$F_k$ fonction de répartition.
\\
On a $F_k(P_M) < \frac12 \implies P_M < \text{med}$.

Sous question: calculer $F_k(x) ?$\\
map: ??
red: ??
map: ??

\textbf{Objectif:} calculer une médiane.

\textbf{Algorithme de base:} on trie les points et on sélectionne un point de
\emph{l'ensemble médian}. En effet, choisir le point central (ou la
moyenne des deux points centraux) serait trop coûteux. \textbf{Cas
particulier:} Dans le cas d'un \emph{échantillon impair}, on acceptera
de prendre un point entre la médiane \(-1\) point et la médiane \(+1\)
point.

\textbf{Complexité de cet algorithme}: \O{n\log n}

Idée: soit \(m_k\) une proposition de médiane pour la classe \(k\).

\subsubsection{Exemple}

Deux classes \(\omega_0\), \(\omega_1\) avec propositions initiales de
médiane respective \(m_0\) et \(m_1\).
\begin{center}
  \begin{tabular}{cc}
    \hline
    \(m_0\) & \(m_1\)\\
    \hline
    20 & 50\\
  \end{tabular}
\end{center}
Si on définit le \emph{mapping} et la \emph{réduction} suivants:
\[
  \left\{
    \begin{matrix}
      \text{map}&: &(x,z) &\mapsto& (z, (\indicatrice{x<m_z}, \indicatrice{x>m_z}))\\
      \text{reduction}&:& ((a_0,b_0),(a_1,b_1)) &\mapsto& (a_0+a_1, b_0+b_1)
    \end{matrix}
  \right.
\]
\textbf{Données et mapping}
\begin{center}
  \begin{tabular}{ccccc}
    \hline
    \(x\) & \(z\) & Mapping & \(z\) & \((a,b)\)\\
    \hline
    12   & 0 & → & \(0\) & \((1,0)\)\\
    24   & 0 & → & \(0\) & \((0,1)\)\\
    14.1 & 0 & → & \(0\) & \((1,0)\)\\
    7    & 0 & → & \(0\) & \((1,0)\)\\
    8    & 1 & → & \(1\) & \((1,0)\)\\
    31.2 & 1 & → & \(1\) & \((1,0)\)\\
    53   & 1 & → & \(1\) & \((0,1)\)\\
  \end{tabular}
\end{center}
\textbf{Après réduction:}
\begin{center}
  \begin{tabular}{c|c}
    \hline
    \(z\) & \(( \sum_i a_i, \; \sum_i b_i)\) \\
    \hline
    \(0\) & \((3,1)\) \\
    \(1\) & \((2,1)\) \\
  \end{tabular}
\end{center}

Nous pouvons voir que la médiane \(m_0\) est trop grande (trois valeurs
en dessous de celle-ci pour une seule au-dessus). En revanche, elle fait
partie de l'intervalle médiane \(\pm 1\) point. On pourrait raffiner par
dichotomie.

\begin{algorithm}
        \caption{\underline{procédureTesterProposition} $(m)$:}
        \label{algo-blabla}
        %\Input{}
        %\Output{}

        $map \gets ((x,z) \mapsto (z, (\indicatrice{z<m_z}, \indicatrice{z>m_z})))$ \;
        $red \gets (x,y \mapsto x+y)$ \;
        $res \gets$ procédureMapReduce($data$, $map$, $red$) \;
        $ret \gets ()$ \;

        \ForEach{$(z,counts) \in res$}{%
            \uIf{$\abs{counts[0]-counts[1]} \leq 1$}{
                $ret[z] \gets 0$ \;
            }
            \uElseIf{$counts[0] < counts[1]$}{
                $ret[z] \gets 1$ \;
            }
            \Else{
                $ret[z] \gets -1$ \;
            }
        }
        return (ret)
\end{algorithm}

\subsection{Recherche du min et max}
Opérations:
\begin{gather*}
  \text{map}: (x,z) \mapsto (z, (x, x))
  \\
  \text{red}: (\underline{a},\bar{a}),(\underline{b},\bar{b}) \mapsto (\min(\underline{a}, \underline{b}), \max(\bar{a}, \bar{b}))
\end{gather*}

\textbf{Commutativité:}
\[
  \left\{
    \begin{matrix}
      \text{red}((\underline{a},\bar{a}),(\underline{b},\bar{b})) & = &(\min(\underline{a}, \underline{b}), \max(\bar{a}, \bar{b}))\\
      \text{red}((\underline{b},\bar{b}),(\underline{a},\bar{a})) & = &(\min(\underline{b}, \underline{a}), \max(\bar{b}, \bar{a}))\\
    \end{matrix}
  \right.
\]
Et par commutativité de \(\max\) et de \(\min\), nous avons directement le résultat.

\textbf{Associativité:}
On a par définition:
 \[
  \left\{
    \begin{matrix}
      \text{red} (\text{red}((\underline{a},\bar{a}),(\underline{b},\bar{b})),(\underline{c},\bar{c})) & = &(\min(\min((\underline{a}, \underline{b}), \underline{c}), \max(\max(\bar{a}, \bar{b}), \bar{c})))\\
      \text{red} ((\underline{a},\bar{a}),(\text{red}((\underline{b},\bar{b}),(\underline{c},\bar{c}))  & = & (\min(\underline{a},\min(\underline{b}, \underline{c})), \max(\bar{a},\max(\bar{b}, \bar{c})))
    \end{matrix}
  \right.
\]
\emph{NB: \(\bar{a}\) (resp. \(\underline{a}\)) est le maximum (resp.
minimum) des \(a\).}
\\
or \(\max\) et \(\min\) sont associatifs ; on a donc :
\[
  \forall (a,b,c), \quad
  \left\{
    \begin{matrix}
      \min(a,\min(b,c)) = \min(\min(a,b),c)\\
      \max(a,\max(b,c)) = \max(\max(a,b),c)
    \end{matrix}
  \right.
\]
ainsi
\[
  \text{red} (\text{red}((\underline{a},\bar{a}),(\underline{b},\bar{b})),(\underline{c},\bar{c})) = \text{red} ((\underline{a},\bar{a}),\text{red}((\underline{b},\bar{b}),(\underline{c},\bar{c})))
\]

\begin{pseudocode}
ProcédureMinMax()
    map ← ...
    red ← ...
    res ← ProcédureMapReduce(data, map, red)
    retourner(res)
\end{pseudocode}

\todo{Mauvais algo}
\begin{algorithm}
    \caption{\underline{ProcédureTrouverMédianes}:}
    \label{algo-blablabla}
    %\Input{}
    %\Output{}

    $bornes \gets$ TrouverMinMax() \;

    \While{$1>0$}{
        \ForEach{$(z, (\underline{b}, \bar{b})) \in bornes$}{%
            $proposition[z] \gets (\underline{b}+\bar{b})/2$ \;
            $res_{propo} \gets$ ProcédureTesterProposition($proposition[z]$) \;
            \ForEach{$(z, (\underline{b}, \bar{b})) \in bornes$}{%
                \uIf{$res_{propo}[z] = 1$}{
                    $bornes[z][1] \gets proposition[z]$\;
                }
                \uElseIf{$res_{propo}[z] = -1$}{
                    $bornes[z][0] \gets proposition[z]$\;
                }
                \Else{
                    $bornes[z][1] \gets bornes[z][0] \gets proposition[z]$\;
                }
            }
        }
    }
\end{algorithm}


\textbf{Complexité des opérations}
\todo{Faux}
\begin{itemize}
  \item \texttt{TrouverMinMax} en \O{n} est complètement parallélisable ;
  \item \texttt{Itérer} en \O{\log n} (algorithme de dichotomie) ;
  \item \texttt{ProcédureTesterProposition} en \O{n} et complètement parallélisable.
\end{itemize}
→ Complexité complète de \texttt{ProcédureTrouverMédianes} en \O{n\log n}.


\subsection{Fractiles}

Soit \(Q \in \N\backslash \{0, 1\}\). On cherche les fractiles
\(\left\{ \frac{q}{Q} \right\}_{q \in [ \![1, Q - 1]\!]}\)

\textbf{Idée:} Un map reduce avec un ensemble de propositions nous
retourne la fonction de répartition empirique dans ses points.

Soit \(t_{k,q}\) le q-ème fractile de la k-ème classe (par exemple le
premier quartile de la 2\textsuperscript{ème} classe) avec \(k\) l'identifiant de classe,
et $q \in \intE1{p-1}$
\[
\text{map}: (x, z) \mapsto \left(z, (1, \indicatrice{x\leq t_{z,1}}, \indicatrice{x\leq t_{z,2}}, \dots, \indicatrice{x\leq t_{z,p-1}})\right)
\]

Exemple pour \(t_{0,1}=3, t_{0,2}=26, t_{0,3}=57\)

\begin{center}
\begin{tabular}{ccccc}
\hline
\(x\) & \(z\) & Mapping & \(z\) &\\
\hline
7 & 0 & → & \(0\) & \((1,0,1,1)\)\\
25 & 0 & → & \(0\) & \((1,0,1,1)\)\\
12 & 0 & → & \(0\) & \((1,0,1,1)\)\\
53 & 0 & → & \(0\) & \((1,0,0,1)\)\\
28 & 0 & → & \(1\) & \((1,0,0,1)\)\\
17 & 0 & → & \(1\) & \((1,0,1,1)\)\\
68 & 0 & → & \(1\) & \((1,0,0,0)\)\\
\end{tabular}
\end{center}
Après réduction :
\begin{center}
  \begin{tabular}{cc}
\hline
\(z\) &\\
\hline
\(0\) & \((7,0,4,6)\)\\
\end{tabular}
\end{center}

\((z, \sum_{z_{i}=z}1, \sum_{z_{i}=z} \indicatrice{x_{i}\leq t_{z,1}}, \sum \dots )\)

on peut ainsi obtenir les images par la fonction de répartition
empirique de la classe \(z\) aux points \((t_{z,j})_j\) ainsi :

\[
\widehat{\mathcal{F}}_z (t_{z,j}) = \frac{\sum_{i \vert z_i=z} \indicatrice{x_i<t_{z, j}}}{\sum_{i \vert z_i=z}1}
\]


\subsection{Régression linéaire}

Contexte: Magasin(année, mois, jour, heure, minute, seconde, x, y, age, dist)\\
On a une supposition sur la distribution de données:
$\text{dist}_i = f(\text{age}_i) + \epsilon_i$
avec $f(x) = \beta_0 + \beta_1 x + \beta_2 x^2$.

Formule: \begin{gather*}
X_h = \left[ 1 | \text{age}_i | \text{age}_i^2 \right]_{i \in S_n} \in \mathcal M_{n,3}
\qquad
Y_h = \left[ \text{dist}_i \right]_{i \in S_n} \in \mathcal M_{n,1}
\end{gather*}

\begin{gather*}
  Y_h = X_h \beta_n + \epsilon\\
  \beta_h = (X_h^\top X_h)^{-1} X_h^\top Y_h
\end{gather*}

Nous pouvons développer le calcul de $\beta_h$:
\begin{align}
  X_n^\top X_n &= \sum_{i \in S_n} (1, \text{age}_i, \text{age}_i^2)^\top (1, \text{age}_i, \text{age}_i^2) \\
&= \sum_{i \in S_n} \begin{pmatrix} 1 & age & age^2 \\ & age^2 & age^3 \\ & & age^4 \end{pmatrix}
\end{align}
Ce qui nous ramène à des réductions (somme de matrices $3 \times 3$).

Exercice : écrire une régression linéaire
$\text{dist} = \text{age} + b \text{age} + c \text{age}^2 + \epsilon$
avec un modèle par heure.
\\
Mapping : $(\dots, h, \dots) \mapsto (h, (
  [1, age, age^2]^\top [1, age, age^2],
  [1, age, age^2]^\top dist)$
\\
Réduction par clef : $((M_1, \theta_1), (M_2, \theta_2)) \mapsto (M_1 + M_2, \theta_1 + \theta_2)$
\\
Mapping final : $\intE0{23} \times (\R^{3\times3}\times \R^3) \to \intE0{23}\times \R^3$
\\$(h, (M, \theta)) \mapsto (h, M^{-1} \theta)$


\subsection{Arbre de décision}

Comment trouver la première frontière ?
$Q(s) = f((n_{cp})_{c \in \{1,2\}, p \in \{\le,>\}})$
Faire une fonction qui calcule la valeur du critère.\\
- objectif pour une variable $k$ on veut calculer\\
$m_{1,\le}^k(s) \quad m_{2,\le}^k(s) \quad m_{1,>}^k(s) \quad m_{2,>}^k(s)$\\
$m_{1,>}^k(s)$: nombre d'éléments de la classe 1 dont $x^{(k)}$ est supérieur à s
\\
Mapping: $\mathbb D \to \mathbb R^4$\\
$(x^{(1)}, x^{(2)}, l) \mapsto (\indicatrice{l=1}\indicatrice{x^{(1)}\le s}, \indicatrice{l=2}\indicatrice{x^{(1)}\le s}, \indicatrice{l=1}\indicatrice{x^{(1)}> s}, \indicatrice{l=2}\indicatrice{x^{(1)}> s}) $
Réduction: +

On est capable de calculer $Q^k(s)$ par $k$ et $s$ connu => on choisit l'argmin (ou du moins une approximation satisfaisante).
À l'itération $j$ on a un classifieur qui à un élément de l'espace envoie un identifiant de partition.


\subsection{Recherche de points le plus proche dans un graphe routier}

Projeter le point qui nous intéresse (notre localisation, par exemple)
sur une composante connexe \emph{agréable}, pas forcément la plus
proche.

Pour un nouveau jeu de points hors de composantes connexes, trouver la
composante connexe la plus agréable (\ie qui contient un point le plus
proche d'un de ces nouveaux points).

\textbf{Données :} \[
\{(x_i, y_i, z_i, t_i, c_i, \dots)\}_i
\] \(x_i\) et \(y_i\) étant les coordonnées géographiques, \(z_i\)
l'altitude, \(t_i\) le ``stuff'' (informations supplémentaires dépendant
de la situation), \(c_i\) une composante connexe.

\textbf{Question :} Trouver par composante connexe les coordonnées du
point le plus proche d'un point \((a,b)\). \emph{Ce genre de question
est susceptible de tomber au final, mais il a choisi un autre exemple.}
Pour cela, on choisit le mapping suivant : \[
    \text{map} : (x,y,t,z,c,...) \mapsto (c, (x,y, |\!|(x,y)-(a,b)|\!|^2))
\]

On choisit la composante connexe \(c_i\) comme clé car on effectuera une
réduction dessus.

\textbf{Données et mapping}

\begin{center}
\begin{tabular}{cccccccc}
\hline
\(x\) & \(y\) & \(z\) & \(t\) & \(c\) & Mapping & \(c\) &\\
\hline
1 & 0 & & & \(0\) & → & \(0\) & \((1,0,4)\)\\
0 & 1 & & & \(1\) & → & \(1\) & \((0,1,2)\)\\
1 & 1 & & & \(0\) & → & \(0\) & \((1,1,5)\)\\
0 & 0 & & & \(1\) & → & \(1\) & \((0,0,1)\)\\
\end{tabular}
\end{center}

On peut alors poser l'opération de réduction suivante : \[
    \text{red} : (x_1, y_1, d_1), (x_2, y_2, d_2) \mapsto \left\{
\begin{matrix}
(x_1,y_1,d_1) \text{ si } d_1 < d_2 \\
(x_2,y_2,d_2) \text{ sinon } \\
\end{matrix}
\right.
\]

Pour commutativité, on doit poser des hypothèse (non équidistance de
points par exemple). Mais avec un peu plus de plomberie, on peut
résoudre le problème.

Cette réduction retourne alors la distance minimum avec les coordonnées
d'un point de la composante \(c\).
