\chapter{Introduction}

  Le stockage de données en haute volumétrie et l'analyse 
  d'icelles\footnote{de celles-ci}
  se feront généralement au travers de modèles de données sortant du cadre
  relationnel, et de modèles de traitement sortant du cadre classique.

  L'objectif de cette introduction sera de présenter les principales propriétés
  du stockage relationnel, et les limitations induites par ces propriétés. Puis
  de présenter de manière formelle les principales propriétés du modèle
  relationnel et des propriétés analogues en sortant de ce cadre.

  \section{Base de données classique}

    Cette section sera traitée au travers d'un exemple qui illustrera les
    différents aspects. Cette introduction n'a pas pour objectif de remplacer un
    cours de base de données relationnelle, elle présuppose ces notions
    acquises.

    \paragraph{Modèle conceptuel de données :}

      \begin{figure}[htbp]
        \centering
        \includegraphics[width=.4\textwidth]{introUVs}
        \caption{Exemple de modèle conceptuel de données}
        \label{f:intro:mcd}
      \end{figure}

      \todo{Clarifier la phrase ci-dessous}
      Ce modèle conceptuel, caractéristique de ce qui se rencontre dans les bases
      de données relationnelles, contient beaucoup d'informations dont l'objectif
      sera de retranscrire ces informations au sein du stockage de données.

    \paragraph{Modèle logique de données :}

      L'objectif du modèle logique de données est de retranscrire au mieux
      toutes les contraintes exprimées dans le modèle conceptuel, de telle
      manière à assurer la cohérence des données.

      Une des manières d'atteindre cet objectif est d'utiliser la
      normalisation, afin de transformer un maximum de dépendances
      fonctionnelles, et contraintes de type clef, contrainte que gère
      nativement un moteur de base de données relationnelle. La réduction de la
      redondance des données n'est qu'un bonus, et n'est pas ce qui est
      recherché en premier dans le processus de normalisation de tout concepteur
      de base de données conscient de l'objet qu'il manipule.
      Il s'agira de comprendre ce que nous allons perdre en passant à de gros
      volumes de données.

      Voici la transcription sous forme de modèle logique de données du modèle
      conceptuel de la figure~\ref{f:intro:mcd}:

      \begin{pseudocode}
      UV(#code, nom, filière → Filière.nom)
      Étudiant(#login)
      Filière(#nom)
      UVÉtudiant(#uv → UV.code, #etu → Étudiant.login)
      UVFilière(#fil → Filière.nom, #uv → UV.code, PCBouPSF bool)
      \end{pseudocode}

      Toutefois ce modèle logique ne retranscrit pas toutes les contraintes
      telles que développées dans le modèle conceptuel. En effet, les contraintes 1\_N
      n'ont pas été retranscrites, uniquement des contraintes 0\_N l'ont été.
      Pour cela, une manière de faire est d'utiliser des déclencheurs, l'implémentation est lourde.

    \paragraph{Exemple de déclencheur (\texttt{postgres}):}

      Cette procédure est souvent utilisée pour s'assurer de l'intégrité des
      données dans le cas de contraintes que l'on ne peut pas directement
      implémenter dans le modèle logique de données.

      Par exemple, pour s'assurer que toutes les UVs ont au moins un étudiant tel
      que décrit dans le modèle conceptuel figure~\ref{f:intro:mcd}, nous
      pouvons écrire la contrainte ensembliste :

      \begin{pseudocodemath}
      Projection(UV, code) |$\subseteq$| Projection(UVÉtudiant, uv)
      \end{pseudocodemath}

      Remarquons au passage que vérifier l'égalité n'est pas nécessaire,
      l'inclusion dans l'autre sens résulte de la contrainte de clef étrangère,
      nous n'avons que ce sens à vérifier.
      Une manière de faire est d'implémenter tout d'abord une requête ne
      retournant rien si la contrainte est satisfaite:
      \begin{mypsql}
      -- Sélection des UVs sans étudiants présents
      SELECT code FROM UV WHERE code NOT IN (SELECT uv from UVEtudiant)
      \end{mypsql}
      On va maintenant créer un trigger pour s'assurer de la contrainte d'intégrité \texttt{1\ldots{}N}:
      \begin{mypsql}
      CREATE FUNCTION funtrig_cl()
          RETURNS trigger AS
      $func$
      BEGIN
          IF EXISTS
              (SELECT code FROM UV
                WHERE code NOT IN
                    (SELECT uv from UVEtudiant))
          THEN RAISE EXCEPTION 'Message disant que tout va mal';
          END IF;
      RETURN NEW;
      \end{mypsql}
      Puis on va dire au moteur de base de données de vérifier cette contrainte
      lors de l'insertion dans \texttt{UV} ou lors de la suppression dans
      \texttt{UVÉtudiant}, et seulement à la fin de la transaction :
      \begin{mypsql}
      CREATE CONSTRAINT TRIGGER trig_cl_UV
      AFTER INSERT OR UPDATE
      ON UV
      DEFERRABLE INITIALLY DEFERRED
      FOR EACH ROW EXECUTE PROCEDURE funtrig_cl();

      CREATE CONSTRAINT TRIGGER trig_cl_UVEtudiant
      AFTER DELETE
      ON UVEtudiant
      DEFERRABLE INITIALLY DEFERRED
      FOR EACH ROW EXECUTE PROCEDURE funtrig_cl();
      \end{mypsql}

      \textbf{Utilisation :}
      Imaginons que l'on rajoute une UV non présente dans la base :
      \begin{mypsql}
      INSERT INTO UV(code) VALUES ('NF26');
      \end{mypsql}
      Une exception sera levée car la contrainte sera violée. Également réaliser
      l'insertion dans l'autre sens ne fonctionne pas non plus, puisque la
      contrainte de clef étrangère est violée:
      \begin{mypsql}
      INSERT INTO UVEtudiant (UV,etu) VALUES ('NF26', 'totopassoir');
      \end{mypsql}
      Il est imposé que l'ensemble des requêtes d'une transaction respecte la
      contrainte, et ainsi les insertions peuvent être réalisées :
      \begin{mypsql}
      BEGIN TRANSACTION
          INSERT INTO UV(code) VALUES ('NF26');
          INSERT INTO UVEtudiant(UV, etu) VALUES ('NF26', 'totopassoir');
      COMMIT
      \end{mypsql}

      L'ensemble des contraintes exprimées dans le modèle conceptuel peuvent être
      implémentées, plus ou moins facilement, dans la base de données, et ainsi
      assurer un stockage toujours cohérent des données.

      On peut néanmoins arriver dans des cas où l'on ne peut passer à
      l'échelle, à cause :

      \begin{itemize}
        \item Du volume des transactions~;
        \item Du volume des données~;
        \item Du volume des données annexes utilisés pour gérer les contraintes
          (indexes).
      \end{itemize}

  \section{Bilan du relationnel}

    Nous pouvons réaliser le bilan suivant du relationnel:

    \paragraph{Relationnel --- Avantages :}

    \begin{itemize}
      \item Intégrité des données ;
      \item Possibilité de faire des requêtes très complexes avec nombre de
        jointures en exploitant les relations ;
      \item Possibilité de faire des requêtes complexes, avec du calcul ;
      \item Standardisé (structure, requêtes...) ;
      \item Grande communauté d'utilisateurs et d'utilisatrices ($\Rightarrow$
        logiciels aboutis).
    \end{itemize}

    \paragraph{Relationnel --- Inconvénients :}

    \begin{itemize}
      \item Gérer un tas d'indexes, ce qui induit:
      \item Volumes peu élevés (même en distribuant) ;
      \item Débit en lecture et en écriture faible (généralement le débit en lecture
        pose moins de problèmes, une distribution peut être mise en place, mais
        l'écriture est plus limitant à cause des contraintes d'intégrité et de la
        génération des indexes) ;
      \item Point unique $\implies$ existence d'un SPOF (\textit{Single Point Of
        Failure}) - un problème sur un seul point (comme un nœud coordinateur,
        par exemple) peut remettre en cause l'intégralité de la base de données.
        De plus cela impose une scalabilité verticale et non horizontale.
    \end{itemize}

  \paragraph{}
  Dans le contexte du \textit{Big data}, un gros volume de données est considéré
  et généralement incompatible avec ces inconvénients.
  Le seul moyen pour amoindrir ces problèmes tout en restant en relationnel
  est de faire du \textit{passage à l'échelle verticale} (c'est-à-dire utiliser des
  serveurs plus gros\ldots{}).

  \textbf{Dans le cadre non relationnel, on va oublier l'utilisation du
  relationnel mais ne pas oublier ses propriétés.}

\section{Formalisation}

  \subsection{Propriétés ACID}

    Ces propriétés sont respectées par les moteurs de base de données
    relationnelle, mais c'est ce qui va nous poser problème.

    \begin{warningBox}
      \begin{itemize}
        \item \textbf{A} : \textbf{\textit{Atomicité}}. Considérer les requêtes comme atomiques :
          l'intégralité d'une transaction est soit acceptée soit rejetée ; un
          ensemble de transactions sera accepté ou rejeté en bloc.
        \item \textbf{C} : \textbf{\textit{Cohérence}} : intégrité des données insérées avec les
          schémas et autres données déjà présentes.
        \item \textbf{I} : \textbf{\textit{Isolation}}. Exécution de transactions en parallèle de
          façon équivalente à une exécution en série.
        \item \textbf{D} : \textbf{\textit{Durabilité}}. Une fois que les données sont
          acceptées, elles vont rester dans la base de données\ldots{} \textit{ad
          vitam æternam} (tant que l'utilisateur n'en décide pas autrement).
      \end{itemize}
    \end{warningBox}

  \subsection{Théorème du CAP ou plutôt Conjecture de Brewer}

    \begin{infoBox}
      Certains l'appellent \textit{Théorème de Brewer} mais celui-ci ne l'a
      jamais démontré, on parlera donc plutôt de \textit{Conjecture de Brewer}.
    \end{infoBox}

    \begin{warningBox}
      \begin{theorem}[Théorème du CAP] L'acronyme « CAP » correspond à:

        \begin{itemize}
          \item \textbf{Consistency : Cohérence}. Si lecture, on obtient
            l'enregistrement le plus récent ou une erreur : pas question de lire
            une donnée absente ou périmée.
          \item \textbf{Availability : Disponibilité}. Chaque lecture
            retourne un résultat, le plus récent ou non.
          \item \textbf{Partition tolerance : Tolérance au partitionnement}. Le système
            continue d'être opérationnel même si une de ses sous-parties est
            absente.
        \end{itemize}
      \end{theorem}
    \end{warningBox}

    \begin{figure}[htbp]
      \centering
      \includegraphics[width=.4\textwidth]{CAP_theorem}
      \caption{Illustration du théorème du CAP}
      \label{f:intro:cap_theorem}
    \end{figure}

    \begin{errorBox}
      \textbf{Théorème/Conjecture :} Le respect de ces trois propriétés
      simultanément est impossible : on ne peut pas construire un système qui
      respecte ces trois propriétés en même temps.
    \end{errorBox}


    En relationnel, on est entre C et A (\ie~le relationnel est tout sauf
    tolérant au partitionnement) là ou en non relationnel, on est soit entre C et
    P, soit entre A et P.

    \begin{figure}[htbp]
      \centering
      \includegraphics[width=.8\textwidth]{CAP_techno}
      \caption{Technologies et théorème du CAP}
      \label{f:intro:cap_techno}
    \end{figure}

    \begin{successBox}
      Le théorème a finalement été démontré par Gilbert et Lynch en 2002,
      \textit{par un formalisme assez pédestre} (voir \cite{illustredCAP}
      pour une explication plus simple. Pour plus détails, on peut se référer au papier original \cite{capTheorem}).
    \end{successBox}

    Preuve du théorème CAP:\\
    On suppose l'existence d'un système respectant CAP, avec $N_1$ et $N_2$ isolés.
    \begin{enumerate}
      \item $C$ écrit $v_1$ sur $N_1$
      \item $N_1$ répond ACK car le système respectant P, il doit fonctionner même isolé.
      \item $C$ lit $N_2$, on a alors 3 possibilités:
        \begin{itemize}
          \item ne répond pas $\implies$ viole P
          \item répond une erreur $\implies$ viole A
          \item répond $v_0 \implies$ viole C
        \end{itemize}
        Ce qui constitue une contradiction, et conclue le raisonnement par
        l'absurde.
    \end{enumerate}

  \subsection{Propriétés BASE}

    Les moteurs de bases de données non relationnelles ne pourront pas respecter
    les propriétés ACID, ainsi, d'autres propriétés ont été introduites, il
    s'agit des propriétés BASE.

    \begin{warningBox}
      \begin{itemize}
        \item \textbf{\textit{Basic Availability}} : Disponibilité la plupart du
          temps. On sait prévoir quand la base de données sera à nouveau
          disponible.
        \item \textbf{\textit{Soft state}} : Pas de cohérence globale au niveau
        des écritures, mais localement oui.
        \item \textbf{\textit{Eventual consistency}} : La cohérence sera
          atteinte à un moment donné. À partir d'un temps fini, la dernière
          donnée est disponible.
      \end{itemize}
    \end{warningBox}
  
\section{Notion de passage à l'échelle}
  Pour améliorer les performances d'une base de données :
  \begin{itemize}
    \item En relationnel, on parle de \textit{scalabilité} (mise à l'échelle) \textit{verticale} en augmentant les performances de la machine hébergeant la BDD.
    \item En non-relationnel, on parle de \textit{scalabilité horizontale} en distribuant les données sur plusieurs machines.
  \end{itemize}

  \begin{warningBox}
    Il existe des systèmes hybrides (comme MySQL) qui proposent à l'utilisateur du relationnel mais qui délèguent une partie du stockage à une BDD non-relationnelle. Le passage à l'échelle n'est cependant pas aussi efficace que du non-relationnel pur.
  \end{warningBox}
